﻿using AppCitasDemoABP.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace AppCitasDemoABP.Controllers
{
    /* Inherit your controllers from this class.
     */
    public abstract class AppCitasDemoABPController : AbpController
    {
        protected AppCitasDemoABPController()
        {
            LocalizationResource = typeof(AppCitasDemoABPResource);
        }
    }
}