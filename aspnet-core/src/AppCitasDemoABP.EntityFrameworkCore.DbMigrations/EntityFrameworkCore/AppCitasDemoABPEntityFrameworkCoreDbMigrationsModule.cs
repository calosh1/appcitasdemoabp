﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace AppCitasDemoABP.EntityFrameworkCore
{
    [DependsOn(
        typeof(AppCitasDemoABPEntityFrameworkCoreModule)
        )]
    public class AppCitasDemoABPEntityFrameworkCoreDbMigrationsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<AppCitasDemoABPMigrationsDbContext>();
        }
    }
}
