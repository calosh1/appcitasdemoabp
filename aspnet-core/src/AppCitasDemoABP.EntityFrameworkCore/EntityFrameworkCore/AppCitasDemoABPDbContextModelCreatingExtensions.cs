﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp;

namespace AppCitasDemoABP.EntityFrameworkCore
{
    public static class AppCitasDemoABPDbContextModelCreatingExtensions
    {
        public static void ConfigureAppCitasDemoABP(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            /* Configure your own tables/entities inside here */

            //builder.Entity<YourEntity>(b =>
            //{
            //    b.ToTable(AppCitasDemoABPConsts.DbTablePrefix + "YourEntities", AppCitasDemoABPConsts.DbSchema);
            //    b.ConfigureByConvention(); //auto configure for the base class props
            //    //...
            //});
        }
    }
}