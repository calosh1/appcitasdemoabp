﻿using Volo.Abp.Localization;

namespace AppCitasDemoABP.Localization
{
    [LocalizationResourceName("AppCitasDemoABP")]
    public class AppCitasDemoABPResource
    {

    }
}