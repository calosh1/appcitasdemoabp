﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;

namespace AppCitasDemoABP.Categorias.Dtos
{
    public class CategoriaDto: EntityDto<int>
    {
        public string Nombre { set; get; }
        public string Descripcion { set; get; }
        public bool IsActive { set; get; }
        public bool IsSector { set; get; }

        public int? CategoriaId { get; set; }
    }

    public class CreateCategoriaDto
    {
        public string Nombre { set; get; }
        public string Descripcion { set; get; }
        public bool IsActive { set; get; }
        public bool IsSector { set; get; }

        public int? CategoriaId { get; set; }
    }
}
