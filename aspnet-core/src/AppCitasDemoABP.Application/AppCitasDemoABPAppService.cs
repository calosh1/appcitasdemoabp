﻿using System;
using System.Collections.Generic;
using System.Text;
using AppCitasDemoABP.Localization;
using Volo.Abp.Application.Services;

namespace AppCitasDemoABP
{
    /* Inherit your application services from this class.
     */
    public abstract class AppCitasDemoABPAppService : ApplicationService
    {
        protected AppCitasDemoABPAppService()
        {
            LocalizationResource = typeof(AppCitasDemoABPResource);
        }
    }
}
