﻿using AppCitasDemoABP.App.Productos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace AppCitasDemoABP.Productos
{
    public class ProductoAppService : CrudAppService<Producto, ProductoDto, int,
        PagedAndSortedResultRequestDto, CreateProductoDto>
    {
        public ProductoAppService(IRepository<Producto, int> repository) : base(repository)
        {
        }
    }

    public class ProductoDto: EntityDto<int>
    {
        public string Name { set; get; }
        public double Price { set; get; }
        public int Quantity { set; get; }
        public ProductType ProductType { set; get; }
    }

    public class CreateProductoDto
    {
        public string Name { set; get; }
        public double Price { set; get; }
        public int Quantity { set; get; }
        public ProductType ProductType { set; get; }
    }
}
