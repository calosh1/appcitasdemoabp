﻿using AutoMapper;

namespace AppCitasDemoABP
{
    public class AppCitasDemoABPApplicationAutoMapperProfile : Profile
    {
        public AppCitasDemoABPApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}
