﻿namespace AppCitasDemoABP.Permissions
{
    public static class AppCitasDemoABPPermissions
    {
        public const string GroupName = "AppCitasDemoABP";

        //Add your own permission names. Example:
        //public const string MyPermission1 = GroupName + ".MyPermission1";
    }
}