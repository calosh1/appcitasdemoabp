﻿using AppCitasDemoABP.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace AppCitasDemoABP.Permissions
{
    public class AppCitasDemoABPPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(AppCitasDemoABPPermissions.GroupName);

            //Define your own permissions here. Example:
            //myGroup.AddPermission(AppCitasDemoABPPermissions.MyPermission1, L("Permission:MyPermission1"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<AppCitasDemoABPResource>(name);
        }
    }
}
