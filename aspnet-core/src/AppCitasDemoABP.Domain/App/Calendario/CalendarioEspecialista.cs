﻿using AppCitasDemoABP.Users;
using ReservaCitas.App.Lugares;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace ReservaCitas.App.Calendario
{
    public class CalendarioEspecialista: Entity<int>
    {
        // FechaInicio
        // FechaFin
        /*
        public int? UsuarioId { set; get; }
        [ForeignKey("UserId")]
        public User User { get; set; }
        */
        [ForeignKey("UserId")]
        public Guid ProfesionalId { set; get; }
        public AppUser Profesional { get; set; }

        [ForeignKey("TipoSemanaId")]
        public int TipoSemanaId { get; set; }
        public virtual TipoSemana TipoSemana { get; set; }

        [InverseProperty("CalendarioEspecialista")]
        public List<JornadaDiaria> JornadaDiaria { set; get; }

        [InverseProperty("CalendarioEspecialista")]
        public Lugar Lugar { set; get; }
    }

    public class TipoSemana: Entity<int>
    {
        public string Nombre { set; get; }
        public string Codigo { set; get; }
        public int DiasLaborables { set; get; }
        public int HorasLaborables { set; get; }
    }

    public class JornadaDiaria: Entity<int>
    {
        public string HoraInicio { set; get; }
        public string HoraFin { set; get; }
        //public TimeSpan

        [ForeignKey("DiaSemanaId")]
        public int DiaSemanaId { set; get; }
        public DiasSemana DiaSemana { set; get; }

        [ForeignKey("CalendarioEspecialistaId")]
        public int CalendarioEspecialistaId { set; get; }
        public CalendarioEspecialista CalendarioEspecialista { set; get; }

        [ForeignKey("TipoJornadaId")]
        public int TipoJornadaId { set; get; }
        public TipoJornada TipoJornada { set; get; }
    }

    public class TipoJornada: Entity<int>
    {
        public string Nombre { set; get; }
        public string Descripcion { set; get; }
    }

    public class DiasSemana : Entity<int>
    {
        public string Nombre { set; get; }
        public string Code { set; get; }
    }
}
