﻿using AppCitasDemoABP.Users;
using ReservaCitas.App.Calendario;
using ReservaCitas.App.Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace ReservaCitas.App.Lugares
{
    public class Lugar: Entity<int>
    {
        public string Nombre { get; set; }
        public string NombreComercial { get; set; }
        public string Referencia { get; set; }
        public string NroLocal { get; set; }

        [ForeignKey("UserId")]
        public Guid? UsuarioId { set; get; }
        public AppUser Usuario { get; set; }

        [ForeignKey("CalendarioEspecialistaId")]
        public int CalendarioEspecialistaId { set; get; }
        public CalendarioEspecialista CalendarioEspecialista { get; set; }

        //Activar luego
        //public virtual ICollection<Servicio> Servicios { get; set; }
    }

    public class ItemLugar: Entity<int>
    {
        public string Nombre { set; get; }
        public string Descripcion { set; get; }
        public string CodigoPostal { set; get; }
        public string Codigotelefono { set; get; }

        public int? ParentId { get; set; }
        [ForeignKey("ParentId")]
        public virtual ItemLugar Parent { get; set; }

        [ForeignKey("TipoLugarId")]
        public int? TipoLugarId { set; get; }
        public TipoLugar TipoLugar { set; get; }
    }

    public class TipoLugar: Entity<int>
    {
        public string Nombre { set; get; }
        public string Descripcion { set; get; }
    }
}
