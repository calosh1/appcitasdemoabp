﻿using AppCitasDemoABP.Users;
using ReservaCitas.App.Lugares;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace ReservaCitas.App.Telefonos
{
    public class Telefono : Entity<int>
    {
               
        
        public int Numero { get; set; }
        public int? Extencion { get; set; }
        public int? CodigoArea { get; set; }
        public int? CodigoPais { get; set; }
        public string? Operadora { get; set; }
        public int Tipo { get; set; }

        [ForeignKey("UserId")]
        public Guid? UsuarioId { set; get; }
        public AppUser Usuario { get; set; }

        [ForeignKey("Lugar")]
        public int? LugarId { get; set; }
        public Lugar Lugar { get; set; }

    }
}
