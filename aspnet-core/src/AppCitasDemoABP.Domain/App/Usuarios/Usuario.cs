﻿using Volo.Abp.Domain.Entities;

namespace ReservaCitas.App.Usuarios
{
    public class TipoUsuario: Entity<int>
    {
        public string Nombre { set; get; }
        public string Descripcion { set; get; }
        public string Codigo { set; get; }

    }

    public class TipoIdentificacion : Entity<int>
    {
        public string Nombre { set; get; }
        public string Descripcion { set; get; }
        public string Codigo { set; get; }

    }

    /*
    public class Usuario: User
    {
        public string Identificacion { set; get; }

        [ForeignKey("TipoIdentificacionId")]
        public int? TipoIdentificacionId { set; get; }
        public TipoIdentificacion TipoIdentificacion { set; get; }

        [ForeignKey("TipoUsuarioId")]
        public int? TipoUsuarioId { set; get; }
        public TipoUsuario TipoUsuario { get; set; }

        [ForeignKey("ItemLugar")]
        public int? PaisId { set; get; }
        public ItemLugar ItemLugar { set; get; }

        [ForeignKey("Provincia")]
        public int? ProvinciaId { set; get; }
        public ItemLugar Provincia { set; get; }



        public virtual ICollection<Categoria> Categorias { get; set; }

        //[IgnoreMap]
        //[InverseProperty("Especialista")]
        //public virtual ICollection<Servicio> Servicios { get; set; }

    }
        */
}
