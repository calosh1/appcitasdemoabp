using AppCitasDemoABP.Users;
using AutoMapper;
using ReservaCitas.App.Categorias;
using ReservaCitas.App.Lugares;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace ReservaCitas.App.Servicios
{
    public class Servicio: Entity<int>
    {
        [ForeignKey("CatalogoServicio")]
        public int CatalogoServicioId { get; set; }
        [IgnoreMap]
        public CatalogoServicio CatalogoServicio { get; set; }

        public string DuracionAtencion { get; set; }
        public float CostoServicio { get; set; }
        public bool ConfirmarCita { get; set; }

        [ForeignKey("Especialista")]
        public Guid EspecialistaId { set; get; }
        public AppUser Especialista { get; set; }

        [ForeignKey("TipoServicio")]
        public int TipoServicioId { get; set; }
        
        [IgnoreMap]
        public ICollection<TipoServicio> TipoServicio { get; set; }

        [InverseProperty("Servicios")]
        public virtual ICollection<Categoria> Categorias { get; set; }

        [IgnoreMap]
        public ICollection<Requisito> Requisitos { set; get; }

        //Activar luego
        //public ICollection<Lugar> Lugares { get; set; }

        public bool IsActive { get; set; }

    }

    public class CatalogoServicio: Entity<int>
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool IsActive { get; set; }

        //[IgnoreMap]
        [InverseProperty("CatalogoServicio")]
        public virtual List<Categoria> Categorias { get; set; }

        public CatalogoServicio()
        {
            this.Categorias = new List<Categoria>();
        }
    }

    public class TipoServicio: Entity<int>
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool IsActive { get; set; }

        [IgnoreMap]
        public ICollection<Servicio> Servicio { get; set; }
    }


    public class Requisito: Entity<int>
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool IsActive { get; set; }
        // Adjunto
        // Formato

        public virtual ICollection<Servicio> Servicios { get; set; }
    }
         
}
