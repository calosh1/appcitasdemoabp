﻿using AppCitasDemoABP.Users;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace ReservaCitas.App.Dependientes
{
    public class Dependiente: Entity<long>
    {
        [ForeignKey("Usuario")]
        public Guid? UsuarioId { set; get; }
        public AppUser Usuario { get; set; }

        [ForeignKey("PersonaDependiente")]
        public Guid? DependienteId { set; get; }
        public AppUser PersonaDependiente { get; set; }
    }
}
