﻿using System.Threading.Tasks;

namespace AppCitasDemoABP.Data
{
    public interface IAppCitasDemoABPDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
