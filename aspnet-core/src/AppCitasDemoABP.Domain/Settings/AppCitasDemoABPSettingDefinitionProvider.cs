﻿using Volo.Abp.Settings;

namespace AppCitasDemoABP.Settings
{
    public class AppCitasDemoABPSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(AppCitasDemoABPSettings.MySetting1));
        }
    }
}
