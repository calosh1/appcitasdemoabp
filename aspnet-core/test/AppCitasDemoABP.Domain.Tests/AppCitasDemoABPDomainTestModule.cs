﻿using AppCitasDemoABP.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace AppCitasDemoABP
{
    [DependsOn(
        typeof(AppCitasDemoABPEntityFrameworkCoreTestModule)
        )]
    public class AppCitasDemoABPDomainTestModule : AbpModule
    {

    }
}