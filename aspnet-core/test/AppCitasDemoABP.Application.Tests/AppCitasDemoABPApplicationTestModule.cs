﻿using Volo.Abp.Modularity;

namespace AppCitasDemoABP
{
    [DependsOn(
        typeof(AppCitasDemoABPApplicationModule),
        typeof(AppCitasDemoABPDomainTestModule)
        )]
    public class AppCitasDemoABPApplicationTestModule : AbpModule
    {

    }
}