import { mapEnumToOptions } from '@abp/ng.core';

export enum ProductType {
  Tipo1 = 1,
  Tipo2 = 2,
  Tipo3 = 3,
}

export const productTypeOptions = mapEnumToOptions(ProductType);
