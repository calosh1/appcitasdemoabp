import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <h1 class="container">Hola mundo</h1>
  <h1>Hola mundo</h1>
  <div>
    <button pButton type="button" label="Click" ></button>
    <p-button label="Click" ></p-button>
  </div>

  <!-- 
    <abp-loader-bar></abp-loader-bar>
    <abp-dynamic-layout></abp-dynamic-layout>
-->
  `,
})
export class AppComponent {}
